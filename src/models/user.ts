export interface iUser {
    username: string;
    password: string;
    userGuid: string;
    level: number;
}
